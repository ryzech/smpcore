# SmpCore

SmpCore is a plugin I created to add certain features my server needs. It may not work for you, but you can try if you want! I'll be updating this pretty regularly so watch out to see if there's anything you might find useful added :] And if you want to request something, open an [issue](https://github.com/ryzech/smpcore/issues/new) and request it there!

# Installation

**Must** have Java 17 installed to compile it, then run this command in the folder.

```bash
./gradlew build
```

# Add as a dependency (May not work)
1. Add the jitpack repo

```groovy
maven { url 'https://jitpack.io' }
```
2. Add it as a dependency in gradle
```groovy
implementation 'com.github.ryzech:smpcore:main-SNAPSHOT'
```

# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
